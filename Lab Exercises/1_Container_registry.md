# Introduction

The **_Advanced CI Lab_** GitLab project is a simple version of a monorepo with 2 separate python applications that exist in the same repository.

Each application gets built and released separately.
 - `python1` has its own `Dockerfile` 
 - `python2` has a separate `Dockerfile`.  
 
A hidden job named **_.build_** is used as the template for the build jobs we define for each application to be built.  This will be the starting point for our lab exercises today.  

The goal of this exercise is to show you how to build and push a containerized application to the container registry. 

An added twist for this scenario is that we must create 2 separate image repositories with different names in the same GitLab container registry.  

We will use the name of the directory that contains the code for each application as the name of the image repository.

## Step 01 - add Docker login, build, and push commands

1. Starting in the **_Advanced CI Lab_** that you forked

- click  **Code > Repository** in the left menu to view the project source code. 
- Notice that there are 2 separate python applications that exist in the same repository
 - `python1` that has its own `Dockerfile`
 - `python2` that has a separate `Dockerfile`.    

2. View the **_.gitlab-ci.yml_** file to review the pipeline that has already been defined. 

- There are two stages, **_build_** and **_test_**
- as well as a hidden **_.build_** job that is used to provide the standard build steps for a python application. 
- Two separate build jobs have been defind, **_build1_** and **_build2_** to build the `python1` and `python2` apps, respectively.

3. Next, go to **_Build>Pipeline Editor_** in the left menu where we will add a variable to the **_.build_** job to define the image name:
```plaintext
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$PROJECT_DIR:$CI_COMMIT_SHA
  ```
 - `$CI_REGISTRY_IMAGE` is a predefined variable that provides the base address for the container registry to push, pull, or tag project’s images.
 - `$PROJECT_DIR` is a variable that is used in each build job to specify which directory to build
 - `$CI_COMMIT_SHA` is a predefined variable that provides the commit sha of the most recent commit that was pushed

4. Next, we will add steps to the **_script_** block of the **_.build_** job to perform the Docker login, Docker build, and Docker push:  
```plaintext
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE
```

The full **_.build_** job should look like this:
```plaintext
.build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$PROJECT_DIR:$CI_COMMIT_SHA
  script:
    - cd $PROJECT_DIR
    - pwd
    - echo "building $PROJECT_DIR"
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE
``` 

5. Finally, **_Commit_** your changes to the main branch.  

- A pipeline should begin as soon as the changes are pushed to the repository.  
- Go to **Build > Pipelines** and click the id of the latest pipeline to view its details. 
- Notice that 2 jobs are executed in the pipeline - one for **_build1_** and a separate one for **_build2_**.  
- Allow these jobs to complete.  

## Step 2 - View the Docker repositories in the container registry

1. Go to **Deploy > Container Registry**.  
- Notice that there are 2 image repositories, one for `python1` and a separate one for `python2`.  
- Click the `advanced-ci-lab/python1` repository to open it and view the details.  
- View the most recent tag.  
- You will notice that this is the same commit sha as the most recent commit that initiated the pipeline.   

### _Need additional help with this exercise? You can see the full .gitlab_ci.yml file solution for this exercise in the `1-container-registry` branch._
